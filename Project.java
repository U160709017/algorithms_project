import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

//Your program will accept two separate unsorted input files of numbers as input.
//Each number is separated by EOL.
//Your program will use LinkedList data structure
//to find and print all the strings
//that occur in the first file but NOT in the second file.
//Then your program will print all the strings that occur in the second file but NOT in the first file. 

public class Project {

	public static void main(String[] args) throws IOException {
		
		File i1 = new File(args[0]);
		File i2 = new File(args[1]);
		
		LinkedList<Integer> list1 = turnLinkedList(i1);
		LinkedList<Integer> list2 = turnLinkedList(i2);

		System.out.println("Elements in the first file but NOT in the second file:");
		for (int i = 0; i < list1.size(); i++) {
			int f = list1.get(i);
			
			if (list2.contains(f)) {
				continue;
			}
			else {
				System.out.print(f + " ");
			}
		}
		
		System.out.println("\n");
		System.out.println("Elements in the second file but NOT in the first file:");
		for (int i = 0; i < list1.size(); i++) {
			int f = list2.get(i);
			
			if (list1.contains(f)) {
				continue;
			}
			else {
				System.out.print(f + " ");
			}
		}
		System.out.println("");
		
	}
	
	static LinkedList<Integer> turnLinkedList(File file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		LinkedList<Integer> linkedList = new LinkedList<Integer>();
		
		String st;
		while ((st = br.readLine()) != null) {
			int i = Integer.parseInt(st);
			linkedList.add(i);
		}
		
		br.close();
		return linkedList;
	}

}




